package com.oreilly.demo.android.pa.uidemo;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.oreilly.demo.android.pa.uidemo.model.Dot;
import com.oreilly.demo.android.pa.uidemo.model.Dots;
import com.oreilly.demo.android.pa.uidemo.view.DotView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


/** Android UI demo program */
public class TouchMe extends Activity {
    /** Dot diameter */
    public static final int DOT_DIAMETER = 30;
    public static final int NUM_MONSTERS = 15;

    /** Listen for taps. */
    private static final class TrackingTouchListener implements View.OnTouchListener {
        private Dots mDots;

        TrackingTouchListener(Dots dots) { mDots = dots; }

        @Override public synchronized boolean onTouch(final View v, final MotionEvent evt) {
            final int action = evt.getAction();
            switch (action & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_POINTER_DOWN:
                    final int idx1 = (action & MotionEvent.ACTION_POINTER_INDEX_MASK)
                            >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                    /*for (Dot dot : mDots.getDots()) {
                        deleteDot(mDots, dot, evt.getX(idx1), evt.getY(idx1));
                    }*/
                    break;
                default:
                    return false;
            }
            return true;
        }

        private void deleteDot(final Dots dots, final Dot dot, final float x, final float y) {
            if ((dot.getX() - x) * (dot.getX() - x) + (dot.getY() - y) * (dot.getY() - y) <= DOT_DIAMETER * DOT_DIAMETER) {
                dots.removeDot(dot);
            }
        }

    }

    private final Random rand = new Random();

    /** The application model */
    private final Dots dotModel = new Dots();

    /** The application view */
    private DotView dotView;

    /** The dot generator */
    private Timer dotGenerator;

    /** Called when the activity is first created. */
    @Override public void onCreate(final Bundle state) {
        super.onCreate(state);

        // install the view
        setContentView(R.layout.main);

        // find the dots view
        dotView = (DotView) findViewById(R.id.dots);
        dotView.setDots(dotModel);

        dotView.setOnCreateContextMenuListener(this);
        dotView.setOnTouchListener(new TrackingTouchListener(dotModel));

        dotModel.setDotsChangeListener((final Dots dots) -> dotView.invalidate());

        runOnUiThread(() -> {
            for (int i = 0; i < NUM_MONSTERS; i++) {
                makeDot(dotModel, dotView, Color.GREEN);
            }
        });

    }

    @Override public void onResume() {
        super.onResume();
        if (dotGenerator == null) {
            dotGenerator = new Timer();
            // every two seconds, move the existing dots, make a new dot, and change
            // the dots' colors
            dotGenerator.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(() -> {
                        moveDots(dotModel, dotView);
                        makeDot(dotModel, dotView, Color.GREEN);
                        changeColors(dotModel);
                    });
                }
            }, 0, 2000);
        }
    }

    @Override public void onPause() {
        super.onPause();
        if (dotGenerator != null) {
            dotGenerator.cancel();
            dotGenerator = null;
        }
    }
    /**
     * @return Boolean expression
     */
    /** Install an options menu. */
    @Override public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.simple_menu, menu);
        return true;
    }
    /**@return Item selected
    /** Respond to an options menu selection. */
    @Override public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_clear:
                dotModel.clearDots();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /** Install a context menu. */
    @Override public void onCreateContextMenu(
            final ContextMenu menu,
            final View v,
            final ContextMenuInfo menuInfo) {
        menu.add(Menu.NONE, 1, Menu.NONE, "Clear").setAlphabeticShortcut('x');
    }
    /** @return context of item selected
     * @param item we are selecting
     */
    /** Respond to a context menu selection. */
    @Override public boolean onContextItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                dotModel.clearDots();
                return true;
            default:
                return false;
        }
    }

    /**
     * @param dots the dots we're drawing
     * @param view the view in which we're drawing dots
     * @param color the color of the dot
     * This method adds a dot to a random location on the view
     */
    void makeDot(final Dots dots, final DotView view, final int color) {
        final int pad = (DOT_DIAMETER + 2) * 2;
        dots.addDot(
                DOT_DIAMETER + (rand.nextFloat() * (view.getWidth() - pad)),
                DOT_DIAMETER + (rand.nextFloat() * (view.getHeight() - pad)),
                color,
                DOT_DIAMETER);
    }

    // This method moves all the dots to new locations on the view
    void moveDots(final Dots dots, final DotView view) {
        final int pad = (DOT_DIAMETER + 2) * 2;
        for (Dot dot : dots.getDots()) {
            dot.setX(DOT_DIAMETER + (rand.nextFloat() * (view.getWidth() - pad)));
            dot.setY(DOT_DIAMETER + (rand.nextFloat() * (view.getWidth() - pad)));
        }
    }

    // This method changes the color of each dot at random

    /**
     * @param dots changes the color of each at random
     */
    void changeColors(final Dots dots) {
        for (Dot dot : dots.getDots()) {
            Random rand = new Random();
            if (rand.nextInt(2) == 0) {
                dot.setColor(Color.YELLOW);
            } else {
                dot.setColor(Color.GREEN);
            }
        }
    }

}