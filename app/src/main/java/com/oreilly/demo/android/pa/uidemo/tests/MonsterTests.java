package com.oreilly.demo.android.pa.uidemo.tests;
import com.oreilly.demo.android.pa.uidemo.view.DotView;
import org.junit.Test;

public abstract class MonsterTests {

public abstract DotView getActivity();

@Test
public void set_up_properly(){
        assert(getActivity() != null);
        }
}